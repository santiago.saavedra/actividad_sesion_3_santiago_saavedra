$(function () {
    var visible = false;

    function display() {
        visible = !visible;
        if (visible) {
            $('p').css('display', 'block');
        } else {
            $('p').css('display', 'none');
        }
    }
    $('#btn-press').click(() => {
        display();
    });
});
